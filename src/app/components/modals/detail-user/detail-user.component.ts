import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {
  }

  getAvatar(genero: string): string{
    if(genero == 'mujer' || genero == 'Mujer' || genero == 'MUJER') return 'assets/female.png';
    else return 'assets/male.png';
  }

  validateItem(item: any): boolean{
    if(!item || item == 'NULL' || item == 'null' || item == null) return false;
    return true;
  }

  validateValue(item: any): any{
    if(!item || item == 'NULL' || item == 'null' || item == null) return 'No posee';
    return item;
  }

  getGender(genero: string): string{
    if(genero == 'mujer' || genero == 'Mujer' || genero == 'MUJER') return 'female';
    else return 'male';
  }

}
