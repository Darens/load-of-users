import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DetailUserComponent } from '../modals/detail-user/detail-user.component';

@Component({
  selector: 'basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.scss']
})
export class BasicTableComponent {
  // Inputs
  @Input() title!: string;
  @Input() keys!: string[];
  @Input() rows!: any;
  @Input() data!: any;

  // Outputs
  @Output() changePage = new EventEmitter<number>();

  constructor(public dialog: MatDialog) {}

  detailUser(data: any) {
    const dialogRef = this.dialog.open(DetailUserComponent, {
      data:{
        item: data,
        keys: this.keys
      }
    });

    // dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    // });
  }

  getPage(page: number): void {
    if((page <= this.data.pages && page >= 1) && page != this.data.page) this.changePage.emit(page);
  }
}
