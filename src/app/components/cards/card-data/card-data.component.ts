import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-data',
  templateUrl: './card-data.component.html',
  styleUrls: ['./card-data.component.scss']
})
export class CardDataComponent implements OnInit {

  @Input() icon: String = 'account_circle';
  @Input() title: String = 'Titulo';
  @Input() data: String = 'data';
  @Input() color: String = 'primary';

  constructor() { }

  ngOnInit(): void {
  }

}
