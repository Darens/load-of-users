import { Component, ViewChild, ElementRef } from '@angular/core';
import * as XLSX from 'xlsx';
import MPage from '@darens/mpage/dist/index.js';
import { DynamicLoaderService } from 'angular-dynamic-loader';
import { MatSnackBar, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';

/**
 * @title Snack-bar with configurable position
 */
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  // Data
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  keys: string[] = [];
  isExcelFile!: boolean;
  mpUsers: any = MPage.index(10); // Inicializando MPage. (El parameto que recibe es la perpaginacion de usuarios)
  users!: any;

  // ViewChilds
  @ViewChild('inputFile') inputFile!: ElementRef;

  // Constructor
  constructor(private _snackBar: MatSnackBar, private loader: DynamicLoaderService) {}

  // Obtener paginacion de usuarios
  getUsers(page: number): void{
    this.users = this.mpUsers.get('users', page, null);
  }

  // Importar usuarios al MPage
  inportData(evt: any) {
    // Documento
    let data: any;
    const target: DataTransfer = <DataTransfer>(evt.target);

    // Validancion de que sea un EXCEL
    this.isExcelFile = !!target.files[0].name.match(/(.xls|.xlsx)/);
    if (target.files.length > 1) {
      this.inputFile.nativeElement.value = '';
    }

    // Si existe un archivo excel, extraemos los datos
    if (this.isExcelFile) {
      this.loader.show();
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        /* save data */
        data = XLSX.utils.sheet_to_json(ws,{
          raw: false,
          dateNF: "dd/mm/yyyy"
        });
        this.mpUsers.save(
          'users', // Key de la store del MPAGE
          data,    // Datos a insertar en la store del MPAGE
          'codigo' // Llave primaria dentro del MPAGE para los datos
        );

        // Obteniendo paginacion de usuarios
        this.getUsers(1);
        this.keys = Object.keys(data[0]);

        // Vaciando el input file
        this.inputFile.nativeElement.value = '';

        this.loader.hide();
      };

      reader.readAsBinaryString(target.files[0]);

    } else {
      this._snackBar.open('El documento debe ser tipo ".xlsx"', 'Cerrar', {
        horizontalPosition: this.horizontalPosition,
        duration: 3000
      });
      this.inputFile.nativeElement.value = '';
      this.loader.hide();
    }
  }
}
