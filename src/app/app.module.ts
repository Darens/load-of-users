import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './material-components/material-module';
import { UsersComponent } from './views/users/users.component';
import { BasicTableComponent } from './components/basic-table/basic-table.component';
import { DetailUserComponent } from './components/modals/detail-user/detail-user.component';
import { CardDataComponent } from './components/cards/card-data/card-data.component';
import { DynamicLoaderModule } from 'angular-dynamic-loader';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    BasicTableComponent,
    DetailUserComponent,
    CardDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    FormsModule,
    DynamicLoaderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
